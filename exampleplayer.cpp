#include "exampleplayer.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    /* 
     * Initializes:
     * mySide and opSide variables
     * lists of:
     *   frontier squares
     *   playable squares
     */
    // Initialize sides
    mySide = side;
    opSide = (side == BLACK) ? WHITE : BLACK;
    
    /*
    // Attempt at initializing frontier and playable
    
    std::cerr << "got here" << std::endl;
    
    // Initialize frontier
    for(int i = 2; i <= 5; i++)
    {
		frontier.push_back(2+8*i);
		std::cerr << "okay still" << std::endl;
		frontier.push_back(5+8*i);
	}
    frontier.push_back(3+8*2);
    frontier.push_back(3+8*5);
    frontier.push_back(4+8*2);
    frontier.push_back(4+8*5);
    // Nonsense about first-pushed thing vanishing
    frontier.push_front(2+8*2);
    
    std::cerr << "made it" << std::endl;
    
    // Initialize playable
    Move* m = new Move(0,0);
    std::list<int>::iterator it;
    
    playable.push_back(-1000);
    
    for(it = frontier.begin(); it != frontier.end(); ++it)
    {
		m->x = (*it) % 8;
		m->y = (*it) / 8;
		std::cerr << m->x << " " << m->y << std::endl;
		if (board.checkMove(m, mySide))
		{
			playable.push_back(*it);
		}
	}
	std::cerr << playable.size() << std::endl;
	delete m;
	*/
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {    
	// initializations
	int i, j;
	Move* move = new Move(0,0);

    // update internal variables
    board.doMove(opponentsMove, opSide);
    
    // If no legal moves, return NULL
    if (!(board.hasMoves(mySide)))
    {
		return NULL;
	}
	
	/*
	// attempt to find the best move, using a heuristic
	int best = -1000;
	Move* bestMove = new Move(0,0);
	std::cerr << "printing" << std::endl;
	for(i = 0; i < 8; i++)
	{
		move->x = i;
		for(j = 0; j < 8; j++)
		{
			move->y = j;
			std::cerr << move->x << " " << move->y << std::endl;
			if(board.checkMove(move,mySide) && (heuristic[i][j] > best))
			{
				std::cerr << "improved score to " << heuristic[i][j] << std::endl;
				best = heuristic[i][j];
				bestMove->x = i;
				bestMove->y = j;
			}
		}
	}
	delete move;
	board.doMove(bestMove, mySide);
	return bestMove;
	*/
	
	// check corners
	for(i = 0; i < 8; i+=7)
	{
		move->x = i;
		for(j=0;j<8;j+=7)
		{
			move->y = j;
			if(board.checkMove(move,mySide))
			{
				board.doMove(move,mySide);
				return move;
			}
		}
	}
	
	// check edges
	// let x be 0 or 7
	for(i = 0; i < 8; i+=7)
	{
		move->x = i;
		for(j=2;j<6;j++)
		{
			move->y = j;
			if(board.checkMove(move,mySide))
			{
				board.doMove(move,mySide);
				return move;
			}
		}
	}
	// let y be 0 or 7
	for(j = 0; j < 8; j+=7)
	{
		move->y = j;
		for(i = 2; i < 6; i++)
		{
			move->x = i;
			if(board.checkMove(move,mySide))
			{
				board.doMove(move,mySide);
				return move;
			}
		}
	}
	
	// check inner stuff
	for(i = 2; i < 6; i++)
	{
		move->x = i;
		for(j = 2; j < 6; j++)
		{
			move->y = j;
			if(board.checkMove(move,mySide))
			{
				board.doMove(move,mySide);
				return move;
			}
		}
	}
	
	// check inner edges
	// let x be 1 or 6
	for(i = 1; i < 7; i+=5)
	{
		move->x = i;
		for(j=2;j<6;j++)
		{
			move->y = j;
			if(board.checkMove(move,mySide))
			{
				board.doMove(move,mySide);
				return move;
			}
		}
	}
	// let y be 1 or 6
	for(j = 1; j < 7; j+=5)
	{
		move->y = j;
		for(i = 2; i < 6; i++)
		{
			move->x = i;
			if(board.checkMove(move,mySide))
			{
				board.doMove(move,mySide);
				return move;
			}
		}
	}
	
	// aw man, we have to play the bad squares
	// inner corners
	for(j = 1; j < 7; j+=5)
	{
		move->y = j;
		for(i = 1; i < 7; i+=5)
		{
			move->x = i;
			if(board.checkMove(move,mySide))
			{
				board.doMove(move,mySide);
				return move;
			}
		}
	}
	// inner edges
	for(i = 0; i < 8; i += 7)
	{
		for(j = 1; j < 7; j += 5)
		{
			move->x = i;
			move->y = j;
			if(board.checkMove(move,mySide))
			{
				board.doMove(move,mySide);
				return move;
			}
			move->x = j;
			move->y = i;
			if(board.checkMove(move,mySide))
			{
				board.doMove(move,mySide);
				return move;
			}
		}
	}
}
