#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include <list>
#include "common.h"
#include "board.h"
using namespace std;

class ExamplePlayer {

private:
    Board board;
	Side mySide;
	Side opSide;
	//std::list<int> frontier;
	//std::list<int> playable;
	/*int heuristic[8][8] = {{10,-5, 5, 5, 5, 5,-5,10},
						   {-5, 0, 0, 0, 0, 0, 0,-5},
						   { 5, 0, 0, 0, 0, 0, 0, 5},
						   { 5, 0, 0, 0, 0, 0, 0, 5},
						   { 5, 0, 0, 0, 0, 0, 0, 5},
						   { 5, 0, 0, 0, 0, 0, 0, 5},
						   {-5, 0, 0, 0, 0, 0, 0,-5},
						   {10,-5, 5, 5, 5, 5,-5,10}};*/

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    Move *doMove(Move *opponentsMove, int msLeft);
};

#endif
