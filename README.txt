What each member did:
Shari designed all the algorithms we decided to attempt.  She did a small amount of debugging, including the most important breakthrough in our assignment (namely, finding out that her computer actually ran the program, unlike Michael's).  She also provided moral support for the coding nonsense that Michael had to endure.

Michael did most of the actual coding - that is, coding the ideas Shari developed and attempting to debug them.  We use the word attempt, because for the most part, it was unsuccessful.

What our AI does:
Our current AI goes by the KISS strategy: Keep It Simple, Silly.  It has a basic heuristic that values corners the most highly, and edges the next-most highly.  Hence, it aggressively tries to play in corners, then tries to prevent the opponent from playing in corners while trying to play in edges.  In our tests, it consistently beat ConstantTimePlayer.

We tried a large number of ideas, but every single one was shot down due to something dumb that happened in trying to implement it:
The first major idea was to make the search of moves more efficient, by implementing some std::list<int>'s to store played squares (squares that have a piece in them), frontier squares (squares that are next to enemy pieces), and playable squares (actual legal moves).  However, we ran into some issues with implementing these.  The first was that for some reason, the first value pushed onto frontier vanished completely.  The second was that after some more attempts, pushing onto any of these lists would cause a segfault.  Neither of these bugs made sense to us, so we moved on.
The second major idea was to code individual squares as "good" or "bad" squares (by an integer), and then having the AI choose the best move based on that.  However, upon attempting to code this up, we could make exactly one legal move.  Then we would get a core dump.  We couldn't figure out why this happened.  Hence, we moved on.
The third major idea was to attempt to implement a "taking" heuristic - namely, moves that take more of the enemy pieces are better.  However, we didn't have time to implement this change, due to spending so long on the previous ideas.

Our other attempts are in our code, commented out.  It would help our sanity if the TAs could look it over and tell us why it doesn't work the way we think it should.